from typing import Optional

import neomodel as nm
from werkzeug.security import generate_password_hash, check_password_hash

TINY_TEXT_LENGTH = 16
VERY_SHORT_TEXT_LENGTH = 32
SHORT_TEXT_LENGTH = 64
MEDIUM_TEXT_LENGTH = 128
LONG_TEXT_LENGTH = 256
VERY_LONG_TEXT_LENGTH = 512


class User(nm.StructuredNode):
    uid = nm.UniqueIdProperty()
    first_name = nm.StringProperty(max_length=LONG_TEXT_LENGTH)
    last_name = nm.StringProperty(max_length=LONG_TEXT_LENGTH)
    username = nm.UniqueIdProperty(max_length=VERY_SHORT_TEXT_LENGTH)
    password_hash = nm.StringProperty(required=True)

    def set_password(self, password: str):
        self.password_hash = generate_password_hash(password)
        return self

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def as_dict(self, exclude_items: Optional[list] = None):
        user_dict = {
            'uid': self.uid,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name
        }
        if exclude_items is not None:
            for item in exclude_items:
                user_dict.pop(item)
        return user_dict


class Photo(nm.StructuredNode):
    uid = nm.UniqueIdProperty()
    caption = nm.StringProperty(max_length=LONG_TEXT_LENGTH)
    tags = nm.JSONProperty()
    published_date = nm.DateTimeProperty()
    drafted = nm.BooleanProperty(default=True)
    src_path = nm.StringProperty(required=True)
    src_sizes = nm.JSONProperty()
    owner = nm.RelationshipTo('User', 'OWNED_BY', cardinality=nm.One)

    def as_dict(self, exclude_items: Optional[list] = None):
        dir(self)
        photo = {
            'uid': self.uid,
            'caption': self.caption,
            'tags': self.tags,
            'src_path': self.src_path,
            'published_date': self.published_date,
            'drafted': self.drafted,
            'src_sizes': self.src_sizes
        }
        if exclude_items is not None:
            for item in exclude_items:
                photo.pop(item)
        return photo


def initialize_database(app):
    neo4j_database_url = app.config['NEO4J_BOLT_URL']
    nm.config.DATABASE_URL = neo4j_database_url

    if app.config.get('NEO4J_AUTO_INSTALL_LABELS') is not None:
        nm.config.AUTO_INSTALL_LABELS = app.config.get(
            'NEO4J_AUTO_INSTALL_LABELS')

    if app.config.get('NEO4J_ENCRYPTED_CONNECTION') is not None:
        nm.config.ENCRYPTED_CONNECTION = app.config.get(
            'NEO4J_ENCRYPTED_CONNECTION')

    if app.config.get('NEO4J_MAX_POOL_SIZE') is not None:
        nm.config.MAX_POOL_SIZE = app.config.get('NEO4J_MAX_POOL_SIZE')

    if app.config.get('NEO4J_FORCE_TIMEZONE') is not None:
        nm.config.FORCE_TIMEZONE = app.config.get('NEO4J_FORCE_TIMEZONE')
