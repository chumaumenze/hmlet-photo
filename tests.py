#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64
import unittest

import pytest

import errors
import models
from app import app
from schema import UserAuthResponse


def get_authorization():
    return base64.b64encode(b"admin:admin").decode("utf-8")


def get_basic_auth_token():
    return base64.b64encode(b"test_user:test_user").decode()


class TestAPI(object):

    def setup_method(self):
        app.debug = True
        self.app = app.test_client()
        self.test_user_pass = 'test_user'
        self.test_user = models.User(
            first_name='test',
            last_name='user',
            username='test_user'
        ).set_password('test_user')
        self.test_user.save()

    def teardown_method(self) -> None:
        self.test_user.delete()

    def test_api_index(self):
        response = self.app.get("/", follow_redirects=True)
        assert response.data.decode('utf-8') == "Photos App – API"
        assert response.status_code == 200

    @pytest.mark.parametrize('context, validator, test_passes', [
        [dict(headers={"Authorization": "Bearer 34ab12cd"}), UserAuthResponse,
         False],
        [dict(headers={"Authorization": "Basic 9854jukd"}), UserAuthResponse,
         False],
        [dict(headers={"Authorization": "Basic"}), UserAuthResponse, False],
        [dict(headers={"": ""}), UserAuthResponse, False],
        [dict(headers={"Authorization": f"Basic {get_basic_auth_token()}"}),
         UserAuthResponse, True]
    ])
    def test_authentication(self, context: dict, validator: UserAuthResponse,
                            test_passes: bool):
        response = self.app.get("/auth", headers=context['headers'])
        try:
            response: UserAuthResponse = validator(**response.json)
        except Exception as e:
            if test_passes:
                raise AssertionError(e)
        else:
            assert response.status == 'success'
            assert response.data.user.dict() == self.test_user.as_dict()

    # TODO: Complete tests for all endpoints

    def test_signup(self):
        pass

    def test_get_all_photos(self):
        pass

    def test_get_photo_by_id(self):
        pass

    def test_update_photo(self):
        pass

    def test_delete_photo(self):
        pass

    def test_get_photo_by_src_path(self):
        pass

    def test_save_photo(self):
        pass

    def test_get_user(self):
        pass

    def test_get_user_photos(self):
        pass


class TestUtils(unittest.TestCase):

    def test_handle_exception(self):
        pass

    def test_build_response(self):
        pass

    def test_remove_image(self):
        pass

    def test_validate_request(self):
        pass

    def test_set_cors_headers(self):
        pass


class TestAPIException(unittest.TestCase):

    def test_api_exception(self):
        message = 'Test message'
        code = 800
        self.api_error = errors.APIException(message, code)
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)

    def test_api_exception_without_args(self):
        message = 'Internal Server Error'
        code = 500
        self.api_error = errors.APIException()
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)


if __name__ == '__main__':
    pytest.main()
