import base64
import datetime
import re

from flask import Flask, render_template, g
from flask import request, current_app, send_from_directory
from flask_jwt_extended import JWTManager, create_access_token
from neomodel import match, Traversal, NodeSet
from neomodel.exceptions import NeomodelException
from werkzeug.datastructures import FileStorage
from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename

from errors import APIException, UserNotFound, InvalidImage, PhotoNotFound
from errors import InvalidCredential
from models import User, Photo
from models import initialize_database
from schema import PhotoQueryArgs, BasicAuthHeader
from schema import SignupBody, PhotoUpdateBody, PhotoUploadBody
from utils import build_response, remove_image, process_image
from utils import set_cors_headers, handle_exception, validate_request

app = Flask(__name__)
app.config.from_pyfile('config.py')
initialize_database(app)
jwt = JWTManager(app)
app.after_request(set_cors_headers)

# Exception handling
error = [
    Exception, APIException, HTTPException, NeomodelException
]
for exc in error:
    app.register_error_handler(exc, handle_exception)


@app.route('/', methods=['GET'])
def photos_app():
    return "Photos App – API"


@app.route('/web', methods=['GET'])
def photos_w_app():
    return render_template('index.html')


# Routes
@app.route('/signup', methods=['POST'])
@validate_request(body=SignupBody)
def signup():
    print(g.req)
    existing_user = User.nodes.first_or_none(username=g.req.body.username)
    if existing_user is not None:
        raise APIException('Username is not available', 400)
    new_user = User(
        first_name=g.req.body.first_name,
        last_name=g.req.body.last_name,
        username=g.req.body.username
    ).set_password(g.req.body.password)
    new_user.save()
    return build_response(
        data=new_user.as_dict()
    )


@app.route('/auth', methods=['GET'])
@validate_request(header=BasicAuthHeader)
def authenticate():
    basic_auth_token = \
    re.findall(r'^Basic (.+)$', g.req.headers.authorization)[0]
    username_password = base64.b64decode(
        basic_auth_token).decode('utf-8').split(':')
    if len(username_password) != 2:
        raise InvalidCredential

    user = User.nodes.first_or_none(username=username_password[0])
    if user is None:
        raise InvalidCredential
    elif not user.verify_password(username_password[1]):
        raise InvalidCredential

    token_expiry = app.config.get(
        'JWT_ACCESS_TOKEN_EXPIRES', datetime.timedelta(hours=1))
    token = create_access_token(identity=user.uid, expires_delta=token_expiry)
    return build_response({
        'user': user.as_dict(),
        'token': token,
        'expires_in': token_expiry.seconds,
        'issued_to': user.uid
    })


@app.route('/photos', methods=['GET'])
@validate_request(query=PhotoQueryArgs)
def get_all_photos():
    req_args: PhotoQueryArgs = g.req.args
    photo_query = Photo.nodes

    if req_args.user is not None:
        user = User.nodes.first_or_none(username=req_args.user)
        if user is None:
            raise UserNotFound
        traversal_route = dict(
            node_class=Photo, direction=match.INCOMING,
            model=None, relation_type=Photo.owner.definition['relation_type'])
        user_photos = Traversal(user, Photo.__label__, traversal_route)
        photo_query = NodeSet(user_photos)

    if req_args.tag is not None:
        photo_query = photo_query.filter(tags__icontains=req_args.tag)

    if req_args.status is not None:
        is_drafted = req_args.status.value == req_args.status.drafted
        photo_query = photo_query.filter(drafted=is_drafted)

    if req_args.order_by is not None:
        order_by = req_args.order_by.value
        if req_args.order is not None and req_args.order == req_args.order.desc:
            order_by = f'-{order_by}'
        photo_query = photo_query.order_by(order_by)

    photos = {'photos': [photo.as_dict() for photo in photo_query.all()]}
    return build_response(photos)


@app.route('/photos/<string:photo_uid>', methods=['GET'])
@validate_request()
def get_photo_by_id(photo_uid: str):
    photo = Photo.nodes.first_or_none(uid=photo_uid)
    if photo is None:
        raise PhotoNotFound
    return build_response(photo.as_dict())


@app.route('/photos/<string:photo_uid>', methods=['PATCH'])
@validate_request(auth_required=True, body=PhotoUpdateBody)
def update_photo(photo_uid: str):
    photo = Photo.nodes.first_or_none(uid=photo_uid)
    if photo is None or not photo.owner.is_connected(g.req.user):
        raise PhotoNotFound
    req_body: PhotoUpdateBody = g.req.body
    if req_body.caption is not None:
        photo.caption = req_body.caption
        hashtags = re.findall(r"#(\w+)", req_body.caption)
        photo.tags = hashtags

    if req_body.publish is not None:
        is_drafted = not req_body.publish
        photo.drafted = is_drafted
        photo.published_date = datetime.datetime.now()

    photo.save()
    return build_response(photo.as_dict())


@app.route('/photos/<string:photo_uid>', methods=['DELETE'])
@validate_request(auth_required=True)
def delete_photo(photo_uid: str):
    user: User = g.req.user
    photo = Photo.nodes.first_or_none(uid=photo_uid)
    if photo is None or not photo.owner.is_connected(user):
        raise PhotoNotFound
    photo.delete()
    photo_files = list(photo.src_sizes.values())
    photo_files.append(photo.src_path)
    remove_image(src_path=photo_files)
    return build_response(message="Photo deleted")


@app.route('/uploads/<path:filename>', methods=['GET'])
@validate_request(auth_required=True)
def get_photo_by_src_path(filename):
    return send_from_directory(current_app.config['UPLOAD_PATH'], filename)


@app.route('/photos', methods=['POST'])
@validate_request(body=PhotoUploadBody, auth_required=True)
def save_photo():
    uploaded_file: FileStorage = request.files['photo']
    filename = secure_filename(uploaded_file.filename)
    if filename != '':
        img_resp = process_image(uploaded_file)
    else:
        raise InvalidImage
    req_body: PhotoUploadBody = g.req.body
    hashtags = re.findall(r"#(\w+)", req_body.caption)
    is_drafted = not req_body.publish
    photo = Photo(
        caption=req_body.caption, tags=hashtags,
        src_path=img_resp['src_path'], src_sizes=img_resp['src_sizes'],
        uid=img_resp['uid'], drafted=is_drafted
    )
    if not photo.drafted:
        photo.published_date = datetime.datetime.now()

    photo.save()
    photo.owner.connect(g.req.user)

    return build_response(photo.as_dict())


@app.route('/users/<username>', methods=['GET'])
@validate_request(auth_required=True)
def get_user(username):
    user = User.nodes.first_or_none(username=username)
    if user is None:
        raise UserNotFound
    return build_response(user.as_dict())


@app.route('/users/<string:username>/photos', methods=['GET'])
@validate_request(auth_required=True)
def get_user_photos(username: str):
    user = User.nodes.first_or_none(username=username)
    traversal_route = dict(
        node_class=Photo, direction=match.INCOMING,
        model=None, relation_type=Photo.owner.definition['relation_type'])
    user_photos = Traversal(user, Photo.__label__, traversal_route)
    if user is None:
        raise UserNotFound
    return build_response({
        'photos': [photo.as_dict() for photo in user_photos]
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
