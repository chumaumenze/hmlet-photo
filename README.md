# README

## Hmlet Photos App API

A simple photos application API

### Features

- Post a photo.
- Save photos as draft.
- Edit photo captions.
- Delete photos.
- List photos (all, my photos, my drafts)
- ASC/DESC Sort photos on publishing date
- Filter photos by user.
- Limit the uploaded photo size to a certain maximum dimensions and bytes.
- JWT authentication.
- Hosted on GCP. Link: [TBD]
- Maintain a good git history.
- Provide a write up to explain the technical choices made.

![2020-07-27_03.10.42.gif](static/img/2020-07-27_03.10.42.gif)

### Requirements

- Python 3.8+
- Pipenv
- Docker Compose

### Technical choices

This tasks uses a Graph database called Neo4j. Alternate database would be a RDBMS such as Postgres or MySQL, even SQLite as the task is for demo purposes. As there is no constraint on which database to use, I chose to use Neo4j primarily for learning purpose. Please do not penalize me for learning.

Regarding image upload and processing, there are multiple ways in which the images can be processed. Currently, the app accepts image files of any size. A constraint on the file size can defined by setting the config variable `MAX_CONTENT_LENGTH`. Once the file is received by the server, it validates the image type, then compress and optimize it. It goes further to create different sizes of the image. On a production server, this approach is inefficient; it is blocking approach thereby it could delay requests for more than split seconds.There are several ways to improve this that includes asynchronous processing, pushing blocking tasks to separate threads, using queues and workers or leveraging event-driven serverless computing.

### Run the Server

For quick start up, run:

```bash
docker-compose up
```

Provide database URI (`NEO4J_BOLT_URL`) in [config.py](./config.py). You can use the following `bolt://100.25.170.168:32904` or [get a sandbox URL](https://sandbox.neo4j.com/). Current [config.py](./config.py) should work by default with docker-compose. Then execute the following:

```bash
pipenv install --ignore-pipfile
pipenv run serve
```

If `pipenv` command is not found, install it by executing `pip install pipenv` followed with the above commands.

API is accessible at `https://localhost:8000`

### Accessing the API

API endpoint: `https://localhost:8000` 

Web interface : `https://localhost:8000/web`

The API exposes protected and unprotected endpoints. The web endpoint can be used on the web browser.

### List of API Endpoints

Endpoints with `Unprotected`  does not require authorization headers. `Protected` endpoints require authorization header with your bearer token except the `/auth` endpoint which uses Basic authentication.

- GET `/` : Test API Connectivity - `Unprotected`
    - Returns: `Photos App – API v1`

- POST `/signup`: Signup for an account - `Unprotected`
    - Requires header with `Content-Type` of `application/json`.

    ```json
    {
        "first_name": "Chuma",
        "last_name": "Umenze",
        "username": "chuma",
        "password": "chuma"
    }
    ```

- GET `/auth`: Get a JSON Web Token required for protected endpoints - `Protected`
    - Requires Basic HTTP authentication token in `Authorization` field of your request headers.

    ```
    Authorization: Basic Y2h1bWE6Y2h1bWE=
    ```

    Returns:

    ```json
    {
      "data": {
        "expires_in": 3600,
        "issued_to": "06fd28e0e2d442549d3d519d389a062b",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTU3ODI1NTcsIm5iZiI6MTU5NTc4MjU1NywianRpIjoiNGQ4NjQzODAtNDBiNy00MDQwLTk2NTAtMGY1MTViMzg0YzJkIiwiZXhwIjoxNTk1Nzg2MTU3LCJpZGVudGl0eSI6IjA2ZmQyOGUwZTJkNDQyNTQ5ZDNkNTE5ZDM4OWEwNjJiIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.WOnqHg0FkMyMHfAspxHNM5v0T9718KsEipzq3rl43Cw",
        "user": {
          "first_name": "Chuma",
          "last_name": "Umenze",
          "uid": "06fd28e0e2d442549d3d519d389a062b",
          "username": "chuma"
        }
      },
      "message": null,
      "status": "success"
    }
    ```

- POST `/photos`: Upload a photo  - `Protected`
    - Requires header with `Content-Type` of  `multipart/form-data`
    - Request body:

    ```json
    caption: "I just #published another #lovely #photo."
    photo: {…}
    publish: "true"
    ```

    - Returns a photo object.

    ```json
    {
      "data": {
        "caption": "I just #published another #lovely #photo.",
        "drafted": false,
        "published_date": "Sun, 26 Jul 2020 14:59:49 GMT",
        "src_path": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert.jpeg",
        "src_sizes": {
          "1080": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w1080.jpeg",
          "320": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w320.jpeg",
          "640": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w640.jpeg",
          "720": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w720.jpeg"
        },
        "tags": [
          "published",
          "lovely",
          "photo"
        ],
        "uid": "58cd70da5ab6424b8e8144de243ed64e"
      },
      "message": null,
      "status": "success"
    }
    ```

- GET`/photos/<string:photo_uid>`: Get a photo by it's unique identifier.  - `Unprotected`
    - Requires a valid photo UID.
    - Returns a photo object.

    ```json
    {
      "data": {
        "caption": "I just #published another #lovely #photo.",
        "drafted": false,
        "published_date": "Sun, 26 Jul 2020 14:59:49 GMT",
        "src_path": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert.jpeg",
        "src_sizes": {
          "1080": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w1080.jpeg",
          "320": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w320.jpeg",
          "640": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w640.jpeg",
          "720": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert_w720.jpeg"
        },
        "tags": [
          "published",
          "lovely",
          "photo"
        ],
        "uid": "58cd70da5ab6424b8e8144de243ed64e"
      },
      "message": null,
      "status": "success"
    }
    ```

- GET `/photos` : Get all photos - `Unprotected`
    - Optional URL params to filter photo. Accepted params:
        - `user` - A user's username (eg. `chuma`)
        - `tag` - Hashtag in captions excluding the hash (eg. `people`)
        - `status` - Either of `published` or `drafted`
        - `order_by` - Either of `published_date` or `uid`
        - `order` - Either of `asc` or `desc`
    - Returns: Multiple photo objects

    ```json
    {
      "data": {
        "photos": [
          {
            "caption": "I am Christina. I am a #teacher and a #father. I just #published a #photo.",
            "drafted": true,
            "published_date": "Sun, 26 Jul 2020 16:47:40 GMT",
            "src_path": "uploads/534d0409d26742bb9be1c1d88cea295e_cs50_cert.jpeg",
            "src_sizes": {
              "1080": "uploads/534d0409d26742bb9be1c1d88cea295e_cs50_cert_w1080.jpeg",
              "320": "uploads/534d0409d26742bb9be1c1d88cea295e_cs50_cert_w320.jpeg",
              "640": "uploads/534d0409d26742bb9be1c1d88cea295e_cs50_cert_w640.jpeg",
              "720": "uploads/534d0409d26742bb9be1c1d88cea295e_cs50_cert_w720.jpeg"
            },
            "tags": [
              "teacher",
              "father",
              "published",
              "photo"
            ],
            "uid": "534d0409d26742bb9be1c1d88cea295e"
          },
          {
            "caption": "I just #published another #lovely #photo.",
            "drafted": false,
            "published_date": "Sun, 26 Jul 2020 14:59:49 GMT",
            "src_path": "uploads/58cd70da5ab6424b8e8144de243ed64e_cs50_cert.jpeg",
            "src_sizes": {
              ...
            },
            "tags": [
              ...
            ],
            "uid": "58cd70da5ab6424b8e8144de243ed64e"
          }
        ]
      },
      "message": null,
      "status": "success"
    }
    ```

- PATCH `/photos/<string:photo_uid>`: Update a photo - `Protected`
    - Returns the updated photo object.

- DELETE `/photos/<string:photo_uid>`: Deletes a photo - `Protected`
    - Returns

    ```json
    {
      "data": null,
      "message": "Photo deleted",
      "status": "success"
    }
    ```

- GET `/users/<string:username>/photos`: Gets all photos by user - `Protected`
    - Returns a photo object

- GET `/user/<string:username>`: Gets the users account profile - `Protected`

### Coverage, Testing & Type checking

To run automated tests, coverage and type checks, execute:

```bash
pipenv install --dev
pipenv run cov-test && pipenv run cov-report ./*.py && pipenv run type-check
```

PS: Tests and coverage may be incomplete.