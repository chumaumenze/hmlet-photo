from enum import Enum
from typing import Optional, Dict, Any

from pydantic import BaseModel, Field

from models import LONG_TEXT_LENGTH


class ResponseModel(BaseModel):
    status: str
    message: Optional[str] = None
    data: Optional[Dict[str, Any]] = None


class UserProp(BaseModel):
    first_name: str
    last_name: str
    username: str = Field(max_length=LONG_TEXT_LENGTH, required=True)
    uid: str


class UserAuthProp(BaseModel):
    token: str
    expires_in: int
    issued_to: str
    user: UserProp


class UserAuthResponse(ResponseModel):
    data: UserAuthProp


class SignupBody(BaseModel):
    first_name: str
    last_name: str
    username: str = Field(max_length=LONG_TEXT_LENGTH, required=True)
    password: str


class PhotoStatus(str, Enum):
    published = 'published'
    drafted = 'drafted'


class OrderBy(str, Enum):
    published_date = 'published_date'
    uid = 'uid'


class Order(str, Enum):
    asc = 'asc'
    desc = 'desc'


class PhotoQueryArgs(BaseModel):
    user: Optional[str]
    tag: Optional[str]
    status: Optional[PhotoStatus]
    order_by: Optional[OrderBy]
    order: Optional[Order]


class PhotoUpdateBody(BaseModel):
    caption: Optional[str] = Field(max_length=LONG_TEXT_LENGTH)
    publish: Optional[bool]


class PhotoUploadBody(PhotoUpdateBody):
    caption: str = Field(max_length=LONG_TEXT_LENGTH)
    photo: Any = Field(...)


class BasicAuthHeader(BaseModel):
    authorization: str = Field(..., regex=r"^Basic .+$")


class JWTBearerHeader(BaseModel):
    authorization: str = Field(..., regex=r"^Bearer .+$")
