from typing import Optional


class APIException(Exception):
    code = 500
    message = "Internal Server Error"

    def __init__(self, message: Optional[str] = None,
                 code: Optional[int] = None):
        self.code = code or self.code
        self.message = message or self.message


class InvalidCredential(APIException):
    code = 401
    message = 'Invalid username or password'


class TokenExpired(APIException):
    code = 498
    message = 'Token expired'


class InvalidToken(APIException):
    code = 401
    message = 'Invalid token'


class InvalidImage(APIException):
    code = 400
    message = 'Please provide a valid image'


class UserNotFound(APIException):
    code = 404
    message = 'No active user found'


class PhotoNotFound(APIException):
    code = 404
    message = 'Photo not found'
