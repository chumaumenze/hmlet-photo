import datetime
import os
import secrets
from typing import Optional, Union

sandbox_db_url = "bolt://neo4j:odds-bet-valves@100.25.170.168:32904"  # expires in 7 days
NEO4J_BOLT_URL: str = os.environ.get('NEO4J_BOLT_URL', sandbox_db_url)
NEO4J_AUTO_INSTALL_LABELS: Optional[bool] = True
NEO4J_FORCE_TIMEZONE: Optional[bool] = False
NEO4J_ENCRYPTED_CONNECTION: Optional[bool] = True
NEO4J_MAX_POOL_SIZE: Optional[int] = 50
NEOMODEL_CYPHER_DEBUG: Optional[bool] = False
UPLOAD_PATH: str = 'uploads'
MAX_CONTENT_LENGTH = 5 * 1_024 * 1_024  # 5MB
JWT_SECRET_KEY: Optional[str] = os.getenv('SECRET_KEY', secrets.token_hex(256))
JWT_ACCESS_TOKEN_EXPIRES: Optional[
    Union[int, datetime.timedelta]] = datetime.timedelta(hours=1)
