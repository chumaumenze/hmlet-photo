import os
import traceback
import uuid
from collections import namedtuple
from functools import wraps
from typing import Union, Callable, Optional, Dict, List, Type, TypeVar, Set

from PIL import Image, UnidentifiedImageError
from flask import make_response, Response, current_app, request, g
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request
from flask_jwt_extended.exceptions import JWTExtendedException
from jwt import PyJWTError, ExpiredSignatureError
from pydantic import BaseModel
from pydantic.error_wrappers import ValidationError
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from errors import APIException, InvalidImage, InvalidToken, TokenExpired
from models import User
from schema import ResponseModel

ALLOWED_UPLOADS_EXT = {'jpeg', 'jpg', 'gif', 'webp', 'png', 'bmp'}
T = TypeVar('T')
ValidatedRequest = namedtuple('ValidatedRequest', 'body headers args user')


def handle_exception(exception: Union[Callable, Exception]):
    code = getattr(exception, 'code', 500)
    traceback.print_exc()
    message = getattr(
        exception, 'description',
        getattr(exception, 'message', APIException.message)
    )

    exception_type = 'failure' if str(code).startswith('4') else 'error'

    return build_response(
        message=message, status=exception_type, code=code)


def build_response(
        data: Optional[Dict] = None, message: Optional[str] = None,
        status: Optional[str] = None, headers: Optional[Dict] = None,
        code: Optional[int] = 200):
    status = status or 'success'
    rv = ResponseModel(status=status.lower(), message=message, data=data)
    response: Response = make_response(rv.dict(), code)
    response.headers.update(headers or {})  # type: ignore
    return response


def process_image(img_src: Union[str, FileStorage],
                  sizes: Optional[List[int]] = None,
                  allowed_img_format: Optional[
                      Union[List[str], Set[str]]] = None,
                  quality: int = 50, optimize: bool = True):
    """Validates, resize, optimize and save image"""
    sizes = sizes or [1080, 720, 640, 320]
    allowed_img_format = allowed_img_format or ALLOWED_UPLOADS_EXT
    src_sizes = {}

    try:
        img = Image.open(img_src)
    except UnidentifiedImageError:
        raise InvalidImage

    if img.format.lower() not in allowed_img_format:
        raise InvalidImage

    filename = secure_filename(getattr(img_src, 'filename', img_src))
    filename, file_ext = os.path.splitext(filename)
    uid = uuid.uuid4().hex
    img_src_path = os.path.join(
        current_app.config['UPLOAD_PATH'],
        f"{uid}_{filename}{file_ext}"
    )
    img_width, img_height = img.size

    for size in sizes:
        if size > img_width:
            continue

        # Scaling by width
        scaled_diff = (img_width - size) / img_width
        scaled_width = int(img_width - (scaled_diff * img_width))
        scaled_height = int(img_height - (scaled_diff * img_height))
        scaled_img = img.resize(
            (scaled_width, scaled_height), Image.ANTIALIAS)
        scaled_img_src_path = os.path.join(
            current_app.config['UPLOAD_PATH'],
            f"{uid}_{filename}_w{size}{file_ext}"
        )
        scaled_img.save(scaled_img_src_path, optimize=optimize,
                        quality=quality)
        src_sizes[size] = scaled_img_src_path
        scaled_img.close()

    # Optimized and save original image
    img.save(img_src_path, optimize=optimize, quality=quality)
    img.close()
    return {'src_path': img_src_path, 'src_sizes': src_sizes, 'uid': uid}


def remove_image(src_path: Union[str, List[str]]):
    if isinstance(src_path, str):
        src_path = [src_path]
    for p in src_path:
        if os.path.exists(p) and os.path.isfile(p):
            os.remove(p)


def validate_request(
        body: Optional[Type[BaseModel]] = None,
        query: Optional[Type[BaseModel]] = None,
        header: Optional[Type[BaseModel]] = None,
        auth_required: bool = False
):
    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            errors = {}
            req = {}
            current_user = None
            if body is not None:
                forms = {**request.form, **request.files}
                request_body = request.get_json() or forms
                try:
                    req['body'] = body(**request_body)
                except ValidationError as e:
                    errors['body'] = e.errors()

            if query is not None:
                request_query_args = request.args
                try:
                    req['query'] = query(**request_query_args)
                except ValidationError as e:
                    errors['query'] = e.errors()

            if header is not None:
                request_headers = {
                    k.replace('-', '_').lower(): v
                    for k, v in request.headers
                }
                try:
                    req['headers'] = header(**request_headers)
                except ValidationError as e:
                    errors['headers'] = e.errors()

            # Validate JWT
            if auth_required:
                try:
                    verify_jwt_in_request()
                except ExpiredSignatureError:
                    raise TokenExpired

                except (JWTExtendedException, PyJWTError):
                    raise InvalidToken
                except ValidationError as e:
                    errors['headers'] = e.errors()
                else:
                    authorized_user_uid = get_jwt_identity()
                    current_user = User.nodes.first_or_none(
                        uid=authorized_user_uid)

            g.req = ValidatedRequest(
                body=req.get('body'), headers=req.get('headers'),
                args=req.get('query'), user=current_user
            )
            if not errors:
                response = func(*args, **kwargs)
            else:
                response = build_response(data=errors, code=400,
                                          status='failure')

            return response

        return wrapper

    return decorate


def set_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = request.headers.get(
        "Origin", "*")
    response.headers["Access-Control-Allow-Credentials"] = "true"

    if request.method == "OPTIONS":
        # Both of these headers are only used for the "preflight request"
        response.headers[
            "Access-Control-Allow-Methods"
        ] = "GET, POST, PUT, DELETE, PATCH, OPTIONS"
        response.headers["Access-Control-Max-Age"] = "3600"  # 1 hour cache
        if request.headers.get("Access-Control-Request-Headers") is not None:
            response.headers["Access-Control-Allow-Headers"] = request.headers[
                "Access-Control-Request-Headers"
            ]
    return response
